/* 
 * File:   ColaEstatica.c
 * Author: eos
 * implementacion de una cola estatica con arreglos
 * Created on 30 de agosto de 2012, 10:12 AM
 */
#include<stdio.h>
#include<stdlib.h>
#define COLAMAX 10
#define TRUE 1
#define FALSE 0

typedef struct Elemento
{
        //informaciom
        int dato;
}elemento;
typedef struct Nodo
{
        elemento nucleo;
}nodo;

typedef struct Cola
{
        int tam;
        nodo Colisima[COLAMAX];
        int frente;
        int final;
}cola;
/*************defienicion de operaciones**************************/
void CrearCola(cola *c);
void Encola(cola *c,nodo e);
elemento desencola(cola *c);
int ColaVacia(cola *c);
int ColaLlena(cola *c);
int TamCola(cola *c);
void ImprimeCola(cola *c);
/****************************************************************/
main()
{
        cola brocoli;
        CrearCola(&brocoli);
        nodo ele;
        int indice;
        int caracter;
        
        puts("hola usando la cola de caracteres hasta");
        printf("%d elementos\n\n",COLAMAX);
        
        for(indice=0;indice<COLAMAX;indice++)
        {
                printf("\nsu caracter numero %d ",indice);
                scanf("%d",&caracter);
                //scanf("%d",&caracter);
                ele.nucleo.dato=caracter;
                Encola(&brocoli,ele);
        }
        //printf("%s",brocoli.Colisima);
        ImprimeCola(&brocoli);
        printf("tamaño de la cola %d \n",TamCola(&brocoli));
        
        for(indice=0;indice<COLAMAX;indice++)
        {
            ele.nucleo=desencola(&brocoli);
            printf("caracter %d\n",ele.nucleo.dato);
            printf("tamaño de la cola %d \n",TamCola(&brocoli));
        }
        
        
}
/**************************implementacion de operaciones*********/
void CrearCola(cola *c)
{
        c->tam=0;
        c->frente=0;
        c->final=0;     
}
void Encola(cola *c,nodo e)
{
        char datote;
        if(ColaLlena(c)==TRUE)
        {
                puts("error cola llena");
        }       
        else
        {
                c->Colisima[c->final].nucleo.dato=e.nucleo.dato;
                datote=(c->Colisima[c->final].nucleo.dato);
                c->final=c->final+1;
                c->tam=c->tam+1;
                //datote=(c->Colisima[c->final].nucleo.dato);
                //printf("%c",datote);
        }
}
elemento desencola(cola *c)
{
        elemento temp;
        if(ColaVacia(c)==TRUE)
        {
                puts("Cola vacia");
        }
        else
        {
                temp=(c->Colisima[c->frente].nucleo);
                c->frente=c->frente+1;
                c->tam=c->tam-1;
                
                return temp;
        }
}
int ColaVacia(cola *c)
{
        if(c->tam==0)
        {
                return TRUE;
        }
        else
        {
                return FALSE;
        }
}
int ColaLlena(cola *c)
{
        if(c->tam == COLAMAX)
        {
                return TRUE;
        }
        else
        {
                return FALSE;
        }
}
int TamCola(cola *c)
{
        return c->tam;
}

void ImprimeCola(cola *c)
{
        int indice=0;
        char info;
        while(indice < COLAMAX)
        {
                info=c->Colisima[indice].nucleo.dato;
                printf("\n%d\n",info);
                indice++;
        }
}

