/*
 * File:   Lista.c
 * Author: eos
 *implementacion de lista dinamica
 * generacion de codigo solo falta nodo
 *
 * Created on 23 de junio de 2012, 12:05 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include<time.h>
//#include "Elemento.c"
#define True 1
#define False 0
typedef struct NODOLISTA
{
    int id;
    elementolista objeto;
    struct NODOLISTA *adelante;//alfrente del nodo siguente elemento
    struct NODOLISTA *atras;//elemento anterior
}nodo;
typedef struct LISTA
{
    int tamano;
	int id;
    nodo *inicio;//inicio de la lista
    nodo *final; //fin de la laista
    nodo CERO;
}lista;
void InicializarLista(lista *l);
int EsVacia(lista *l);//regresara un booleano verdadero si la lista esta vacioa en caso contrario regresara un falso
nodo * Primero(lista *l);//primer elemento de la lista regresa un apuntador
nodo * Ultimo(lista *l);//ultimo elemento de la lista regresa un apuntador
void InsertarFinal(lista *l,elementolista nucleo);//inserta al final de la lista
void InsertarInicio(lista *l, elementolista nucleo);//inserta un nodo al inicio de la lista
nodo * Siguiente(lista *l,nodo *Referencia);//nodo siguiente del que se tiene la referencia
nodo * Anterior(lista *l,nodo *Referencia);//nodo anterior al quese selecciono
void InsertarFrente(lista *l,nodo *Referencia,elementolista nucleo);//con relacion al nodo base
void InsertarAtras(lista *l,nodo *Referencia,elementolista nucleo);//con relacion al nodo base
//nodo* Localizar(lista *l,elementolista nucleo);//busca el elemento//no se requiere es mas facil usar una funcion dentro de la implementeacion
void Suprimir(lista *l,nodo *Referencia);//elimina un nodo
void BorrarLista(lista *l);//eleminia la lista
nodo * CrearNodoLista(void);
int TamLista(lista * l);//obtiene el tamaño de la lista

/***************************Funciones***************************************************/
int TamLista(lista * l)
{
    return l->tamano;
}
//eleminia la list
void BorrarLista(lista *l)
{
    //nodo *temp;
    if(EsVacia(l)==True)
    {
        printf("\n***La lista esta vacia no se puede eliminar***\n");
    }
    else
    {
        while(l->final==&l->CERO)
        {
            Suprimir(l,l->final);
        }
    }
}
//elimina un no
void Suprimir(lista *l,nodo *Referencia)
{
    if(l->id==Referencia->id)
    {
        if(EsVacia(l)==True)
        {
            printf("\n***La lista esta vacia no se puede eliminar nada***\n");
        }
        else if(l->inicio==Referencia)//por si esta el nodo al inicio
        {
            l->inicio=Referencia->adelante;
            Siguiente(l,Referencia)->atras=&l->CERO;
            free(Referencia);
            l->tamano--;
        }
        else if(l->final==Referencia)//por si esta al final el nodo
        {
            l->final=Referencia->atras;
            Anterior(l,Referencia)->adelante=&l->CERO;
            free(Referencia);
            l->tamano--;
        }
        else//por si esta en el centro de la lista
        {
            Anterior(l,Referencia)->adelante=Referencia->adelante;
            Siguiente(l,Referencia)->atras=Referencia->atras;
            free(Referencia);
            l->tamano--;
        }
    }
    else
    {
        printf("\n***no se puede generar el borrado el la referencia no pretenece a la lista***\n");
    }

}

//con relacion al nodo base en nuevo nodo se inserta detras en teoria a la izquierda
void InsertarAtras(lista *l,nodo *Referencia,elementolista nucleo)
{
    nodo *NuevoNodo;
    NuevoNodo=CrearNodoLista();
    NuevoNodo->objeto=nucleo;
    if(l->id==Referencia->id)
    {
        if(EsVacia(l)==True)
        {
            InsertarInicio(l,nucleo);
        }
        else if(Referencia->atras==&l->CERO)//para checar si el nodo referencia esta al frente de mi lista y el nuevo nodo se agrege al inicio de lal lista
        {
            InsertarInicio(l,nucleo);
        }
        else//si el nodo referencia esta en cualquier otro lugar de la lista que no sea inicio y final se agrege normalmente  a la lista
        {
            NuevoNodo->atras=Referencia->atras;
            NuevoNodo->adelante=Anterior(l,Referencia)->adelante;
            Referencia->atras=NuevoNodo;
            Anterior(l,NuevoNodo)->atras=NuevoNodo;
            l->tamano++;
            NuevoNodo->id=l->id;
        }
    }
    else
    {
        printf("\n***tu nodo referencia no pertenece a la lista indicada ***\n");
    }
/*
 *opcion corta
 *    ademas de ahorrar la declaracion de un nuevo nodo almenos en esta funcion
 *       Referencia=Anterior(l,Referencia);
 *       InsertarFrente(l,Referencia,nucleo);
 *       l->tamano++;
 *       NuevoNodo->id=l->id;
 * opcion larga
 *       NuevoNodo->atras=Referencia->atras;
 *       NuevoNodo->adelante=Anterior(l,Referencia)->adelante;
 *       Referencia->atras=NuevoNodo;
 *       Anterior(l,NuevoNodo)->atras=NuevoNodo;
 *       l->tamano++;
 *       NuevoNodo->id=l->id;
 */
}
//con relacion al nodo base se inserta el nuevo nodo al frente en teoria a la derecha
void InsertarFrente(lista *l,nodo *Referencia,elementolista nucleo)
{
    nodo *NuevoNodo;
    NuevoNodo=CrearNodoLista();
    NuevoNodo->objeto=nucleo;
    if(l->id==Referencia->id)
    {
        if(EsVacia(l)==True)//para checar e insertarlo al inicio de la listas
        {
            InsertarInicio(l,nucleo);
        }
        else if(Referencia->adelante==&l->CERO)//para checar si el elemento de referencia es el ultimo o apunta al nodo cero intruduzca el nuevo nodo al final de la lista
        {
            InsertarFinal(l,nucleo);
        }
        else//si el elemento de referencia enta a la mitad de la lista genera el agregado del nuevo nodo
        {
            NuevoNodo->adelante=Referencia->adelante;
            NuevoNodo->atras=Siguiente(l,Referencia)->atras;
            Referencia->adelante=NuevoNodo;
            Siguiente(l,Referencia)->atras=NuevoNodo;
            l->tamano++;
            NuevoNodo->id=l->id;
        }
    }
    else
    {
        printf("\n***el elemento de referencia no es de la lista ***\n");
    }
/*
    NuevoNodo->adelante=Referencia->adelante;
    NuevoNodo->atras=Siguiente(l,Referencia)->atras;
    Referencia->adelante=NuevoNodo;
    Siguiente(l,Referencia)->atras=NuevoNodo;
*/
}
//inserta un nodo al inicio de la lista
void InsertarInicio(lista *l, elementolista nucleo)
{
    nodo *NuevoNodo,*NodoPrimero;
    NuevoNodo=CrearNodoLista();
    NuevoNodo->objeto=nucleo;
    if(EsVacia(l)==True)
    {
        l->inicio=NuevoNodo;
        NuevoNodo->atras=&l->CERO;
        NuevoNodo->adelante=&l->CERO;
        l->final=NuevoNodo;
        NuevoNodo->id=l->id;
    }
    else
    {
        NodoPrimero=Primero(l);
        NuevoNodo->adelante=NodoPrimero;
        NuevoNodo->atras=&l->CERO;
        l->inicio=NuevoNodo;
        NuevoNodo->id=l->id;
    }
    l->tamano++;
}
//se inserta elementos al final de la lista
void InsertarFinal(lista *l,elementolista nucleo)
{
    nodo *NuevoNodo,*NodoUltimo;
    NuevoNodo=CrearNodoLista();
    if(EsVacia(l)==True)
    {
        InsertarInicio(l,nucleo);
    }
    else
    {
        NuevoNodo->objeto=nucleo;
        NodoUltimo=Ultimo(l);
        NodoUltimo->adelante=NuevoNodo;
        NuevoNodo->adelante=&l->CERO;
        NuevoNodo->atras=NodoUltimo;
        l->tamano++;
        NuevoNodo->id=l->id;
    }
/*
    NuevoNodo->objeto=nucleo;
    NodoUltimo=Ultimo(l);
    NodoUltimo->adelante=NuevoNodo;
    NuevoNodo->adelante=&l->CERO;
    NuevoNodo->atras=NodoUltimo;
    l->tamano++;
*/
}
//nodo anterior al quese selecciono
//solo se pide la lista como referencia o como etiqueta
nodo * Anterior(lista *l,nodo *Referencia)
{
     if(l->id==Referencia->id)
    {
         if(Referencia->atras==&l->CERO)
         {
             printf("\n***Se termino la lista atras***\n");
         }
         else
         {
             return Referencia->atras;
         }
         
    }
    else
    {
        printf("\n***el elemento de referencia no peretenece al la lista***\n");
    }
}
//nodo siguiente de la lista
//solo se pide la lista como referencia o como etiqueta
nodo * Siguiente(lista *l,nodo *Referencia)
{
    if(l->id==Referencia->id)
    {
        if(Referencia->adelante==&l->CERO)
         {
             printf("\n***Se termino la lista alfrente ***\n");
         }
        else
        {
            return Referencia->adelante;
        }
    }
    else
    {
        printf("\n***el elemento de referencia no peretenece al la lista***\n");
    }
}
//adquiere la direccion del primer elemento de la lista
nodo * Primero(lista *l)
{
    if(EsVacia(l)==True)
    {
        printf("\n***Lista Vacia***\n");
    }
    else
    {
        return l->inicio;
    }
}
//adquiere la diereccion del ultimo elemento de la lista
nodo * Ultimo(lista *l)
{
    if(EsVacia(l)==True)
    {
        printf("\n***Lista Vacia***\n");
    }
    else
    {
        return l->final;
    }
}
//inicializa una lista
void InicializarLista(lista *l)
{
    l->inicio=&l->CERO;
    l->final=&l->CERO;
    l->tamano=0;
	l->id=rand()%10000;
    //return 0;
}
//verifica si la lista es vacia
int EsVacia(lista *l)
{
    if((l->tamano==0) && (l->final==&l->CERO) && (l->inicio==&l->CERO) )
    {
        return True;
    }
    else
    {
        return False;
    }
}
//genera nodos para ser insertados
nodo * CrearNodoLista(void)
{
    nodo *temp;
    temp=malloc(sizeof(nodo));
    temp->adelante=NULL;
    temp->atras=NULL;
    return temp;
}


