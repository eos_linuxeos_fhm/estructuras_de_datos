/*
 * File:   Uso.c
 * Author: eos
 *
 * Created on 24 de junio de 2012, 05:54 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Elemento.h"
#include "Lista.h"
#include "Pila.h"

void imprilist(lista *l);

/*
 *
 */
main()
{
    puts("hola mundo\n\n");

    lista lista1;
    pila pila1;
    int indice,val,val2, valist1=0, valist2=50,tam;
    elementopila objetote;
    elementolista objetito;
    nodo *nodolistas,Referencialista;



    InicializarLista(&lista1);

    printf("Uso de la lista lista vacia suprimir ");
    Suprimir(&lista1,&Referencialista);
    puts("borrar lista vacia ");
    BorrarLista(&lista1);





    objetito.numerote=valist1;
    InsertarInicio(&lista1,objetito);
    objetito.numerote=valist2;
    InsertarFinal(&lista1,objetito);
    imprilist(&lista1);



    printf("tamaño de la lista con dos nodos: %d\n\n",TamLista(&lista1));




    nodolistas=Primero(&lista1);
    puts("seccion de error ");
    nodolistas=Siguiente(&lista1,nodolistas);// tiene que mandar error
    puts("regresamos la direccion del primer elemento ");
    nodolistas=Primero(&lista1);

    puts("ciclo");
    tam=TamLista(&lista1);
    tam=49;
    for(indice=1;indice<=tam;indice++)
    {
        objetito.numerote=indice;
        InsertarFrente(&lista1,nodolistas,objetito);
        nodolistas=Siguiente(&lista1,nodolistas);
    }
    printf("tamaño de la lista despues del cliclo: %d\n\n",TamLista(&lista1));

    imprilist(&lista1);



    printf("borrar lista");
    BorrarLista(&lista1);
    puts("termina la lista ");


//uso de la pila
    puts("comienza la pila");
    CrearPila(&pila1);
   //estraer un elemento sin nada en la pila
    objetote=Pop(&pila1);


    for(indice=0;indice<10;indice++)
    {
        printf("valor de la insercion %d\n",indice);
        objetote.numero=indice;
        Push(&pila1,objetote);
    }

    printf("\ntamño pila: %d\ncomiensa a salir los datos\n\n",Tam_pila(&pila1));
    val2=Tam_pila(&pila1);

    for(indice=0;indice<val2;indice++)
    {
        objetote=CopiaTope(&pila1);
        val=objetote.numero;
        printf("indice: %d valor antes de sacarlo es la copia %d \n",indice,val);
        objetote=Pop(&pila1);
        val=objetote.numero;
        printf("valor de la extraccion %d\n",val);
    }

    puts("se genera la eliminacion");
    EliminaPila(&pila1);
    puts("termino del programa");

}


void imprilist(lista *l)
{
    nodo *base;
    int indice,tam;
    base=Primero(l);
    tam=TamLista(l);
    puts(" ");
    for(indice=0;indice<tam;indice++)
    {
        printf("\n%d,%d\n",indice,base->objeto.numerote);
        base=Siguiente(l,base);
    }
    puts(" ");
    //printf("%d",base->objeto.numerote);

}
