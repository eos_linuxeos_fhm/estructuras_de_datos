/* 
 * File:   PilaEstatica.c
 * Author: eos
 *
 * Created on 23 de agosto de 2012, 10:22 AM
 */

#include <stdio.h>
#include <stdlib.h>
#define MAXIMO 100000

#define TRUE 1
#define FALSE 0

typedef struct ELEMENTO
{
    int numero;
    //informacion
}elemento;
typedef struct NODO
{
    elemento dato;
}nodo;
typedef struct PILA
{
    nodo Duracell[MAXIMO+1];
    int tope;
    int base;
    int id;
    int tam;
}pila;

/*------------prototipos--------------*/
void CrearPila(pila *p);
int PilaVacia(pila *p);
int PilaLlena(pila *p);
int TamPila(pila *p);
void Push(pila *p,nodo info);
nodo Pop(pila *p);
nodo Tope(pila *p);
/*
 * 
 */
main()
{
    pila acumulador;
    int indice;
    nodo numerote;
    
    CrearPila(&acumulador);
    
    for(indice=0;indice<MAXIMO;indice++)
    {
        
        numerote.dato.numero=indice;
        
        Push(&acumulador,numerote);
        
    }
    printf("\n\ntamaño pila %d\n\n",TamPila(&acumulador));
    printf("\n\n mostrando tope %d\n\n ",Tope(&acumulador).dato.numero);
    
    for(indice=0;indice<MAXIMO;indice++)
    {
        
        numerote.dato.numero=indice;
        
        numerote=Pop(&acumulador);
        printf("\n%d\n",numerote.dato.numero);
    }
    //printf("\n\n mostrando tope %d\n\n ",Tope(&acumulador).dato.numero);
}
/*****************Funciones**********************/
nodo Pop(pila *p)
{
    nodo temporal;
    if(PilaVacia(p)==TRUE)
    {
        printf("pila vacia del pop");
    }
    else
    {
        temporal=p->Duracell[p->tope];
        p->tope=p->tope-1;
        return temporal;
    }
}
void Push(pila *p, nodo info)
{
    if(PilaLlena(p)==TRUE)
    {
        puts("la pila esta llena");
    }
    else
    {
        p->Duracell[p->tope+1].dato=info.dato;//checar esta parte
        p->tope=p->tope+1;
        p->tam=p->tam+1;
    }
}
nodo Tope(pila *p)//solo extrae una copia del tope no lo desempila
{
    if(PilaVacia(p)==TRUE)
    {
        puts("pila vacia de funcion tope");
        
    }
    else
    {
        return p->Duracell[p->tope];
    }
}
void CrearPila(pila *p)
{
    static int identificador=0;
    p->base=0;
    p->tope=0;
    p->tam=0;
    p->id=identificador+1;
}
int PilaVacia(pila *p)
{
    if(p->tope==0 && p->base==0 )
    {
        return TRUE;
    }
    else
    {
        return FALSE;        
    }
}
int PilaLlena(pila *p)
{
    if(p->tope==MAXIMO)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}
int TamPila(pila *p)
{
    return p->tam;
}
/************************************************/


