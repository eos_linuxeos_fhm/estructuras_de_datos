/* 
 * File:   arboles.c
 * Author: eos
 *
 * Created on 1 de julio de 2012, 08:11 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define True 1
#define False 0


typedef struct NODOARBOL
{
    int id;
    elementoarbol objeto;
    struct NODOARBOL *derecha;
    struct NODOARBOL *izquierda;    
}nodoarbol;
typedef struct ARBOL
{
    int tamano;
    int id;
    nodoarbol *raiz;//inicio de la lista
    nodoarbol RAIZCERO;
}arbol;


void CrearArbol(arbol *a);
int EsVacio(arbol *a);
nodoarbol* Izquierdo(arbol *a, nodoarbol *Referencia);
nodoarbol* Derecho(arbol *a,nodoarbol *Referencia);
nodoarbol* CrearNodoArbol(void);

void Construir(arbol *a,arbol *izquierod,elementoarbol nucleo,arbol *derecho);

nodoarbol* Raiz(arbol *a);
//nodoarbol* NodoPapa(arbol *a, nodoarbol *Referencia);
//void BorrarArbol(arbol *a);
//void BorrarNodo(arbol *a,nodoarbol *Referencia);
//int Pertenece(arbol *a);

/*
 ***************************************************************************************************
 ***************************************************************************************************
 ***************************************************************************************************
 */

//obtiene la raiz del arbol 
nodoarbol* Raiz(arbol *a)
{
    return a->raiz;
}

//crea o une un arbol
void Construir(arbol *a,arbol *izquierdo,elementoarbol nucleo,arbol *derecho)
{
    nodoarbol *temp;
    temp=CrearNodoArbol();
    temp->objeto=nucleo;
    a->raiz=temp;
    if(izquierdo==NULL && derecho==NULL)//por si no recibe elelemntos derechos ni izquerdos
    {
        temp->izquierda=NULL;
        temp->derecha=NULL;
    }
    else if(izquierdo==NULL && derecho !=NULL)//por si la incerccion sera en el lado derecho
    {
        temp->izquierda=NULL;
        temp->derecha=Raiz(derecho);
    }
    else if(izquierdo !=NULL && derecho==NULL)//por si la serccion solo sera la de la izquierda
    {
        temp->derecha=NULL;
        temp->izquierda=Raiz(izquierdo);
    }
    else//es el caso general que reciba ambos elementos derechos e izquierdos
    {
        temp->izquierda=Raiz(izquierdo);
        temp->derecha=Raiz(derecho);
    }
//    if(izquierdo==NULL && derecho==NULL)//por si no recibe elelemntos derechos ni izquerdos
//    {
//        temp->izquierda=&a->RAIZCERO;
//        temp->derecha=&a->RAIZCERO;
//    }
//    else if(izquierdo==NULL && derecho !=NULL)//por si la incerccion sera en el lado derecho
//    {
//        temp->izquierda=&a->RAIZCERO;
//        temp->derecha=Raiz(derecho);
//    }
//    else if(izquierdo !=NULL && derecho==NULL)//por si la serccion solo sera la de la izquierda
//    {
//        temp->derecha=&a->RAIZCERO;
//        temp->izquierda=Raiz(izquierdo);
//    }
//    else//es el caso general que reciba ambos elementos derechos e izquierdos
//    {
//        temp->izquierda=Raiz(izquierdo);
//        temp->derecha=Raiz(derecho);
//    }
}
//indica si el arbol esta vacion 
int EsVacio(arbol *a)
{
    if(a->raiz==NULL && a->tamano==0 )
    {
        return True;
    }
    else
    {
        return False;
    }
}
//da la direccion del nodo izquierod
nodoarbol* Izquierdo(arbol *a,nodoarbol *Referencia)
{
    if(a->id==Referencia->id)
    {
        return Referencia->izquierda;
    }
    else
    {
        printf("el elemento de referencia no pertenece al arbol");
    }
}
//da la direccion del nodo derecho
nodoarbol* Derecho(arbol *a,nodoarbol *Referencia)
{
    if(a->id==Referencia->id)
    {
        return Referencia->derecha;
    }
    else
    {
        printf("el elemento de referencia no pertenece al arbol");
    }
}
//inicializa un arbol
void CrearArbol(arbol *a)
{
    a->raiz=NULL;
    a->id=rand()%10000;
    a->tamano=0;
}
//genera nodos para ser insertados
nodoarbol * CrearNodoArbol(void)
{
    nodoarbol *temp;
    temp=malloc(sizeof(nodoarbol));
    temp->derecha=NULL;
    temp->izquierda=NULL;
    return temp;
}

/*
 * 
 */
/*

main() 
{
    printf("hola mundo");    
}
//borra todos los elementos de un arbol
void BorrarNodo(arbol *a,nodoarbol *Referencia)
{
    nodoarbol temp;
    arbol arboltemporal;
    if(a->raiz==Referencia)//por si mi elemento esta en la raiz del arbol y los dos hijos
    {
        arboltemporal->raiz=Referencia->derecha;
    }
    else if(Referencia->derecha==&a->RAIZCERO && Referencia->izquierda==&a->RAIZCERO)//por si la referencia es la raiz y no tiene elementos o es el ultimo elemento 
    {
        free(Referencia);
    }
}

//obtiene el nodo papa del elemento referencia
nodoarbol* NodoPapa(arbol *a, nodoarbol *Referencia)
{
    //buscar el nodo correspondiente;
}
*/
