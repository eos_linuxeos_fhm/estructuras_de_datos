/*
 * File:   Lista.c
 * Author: eos
 *implementacion de una pila dinamica
 *
 *
 * Created on 23 de junio de 2012, 12:05 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include "Elemento.c"
#define True 1
#define False 0

typedef struct ELEMENTOPILA
{
    nodoarbol *root;
    //int numero;
    //informacion
}elementopila;
typedef struct NODOPILA
{
    int id;
    elementopila objetopila;//es la informacion que contiene la pila o que sera alamcenada en la pila
    struct NODOPILA *abajo;//genera la referencia del elemento siguente de la pila


}nodopila;

typedef struct PILA
{
    int tamano;
    nodopila * tope;
    nodopila *apu_base;
    nodopila base;

}pila;

/*******prototipos de funciones**********/
nodopila* CrearNodoPila(void);
void CrearPila(pila *p);//crea una pila par su uso
int PilaVacia(pila *p);//verifica si la pila esta vacia
int Tam_pila(pila *p);//obtiene el tamaño de la pila
void Push(pila *p,elementopila nucleo);//agrega un elemento a la pila
elementopila Pop(pila *p);//extrae el ultimo elemento de la pila
void EliminaPila(pila *p);//elimina toda la pila
elementopila CopiaTope(pila *p);//extrae una copia del elemento tope de la pila



/************************Funciones****************************************
/*
 * extrae una copia del elemento tope de la pila
 */
elementopila CopiaTope(pila *p)
{
    nodopila *temp;
    temp=p->tope;
    return temp->objetopila;

}

 //*elimina toda la pila
void EliminaPila(pila *p)
{
    elementopila temp;
    while(!PilaVacia)
    {
        temp=Pop(p);
    }
    CrearPila(p);

}


 //*extrae el ultimo elemento de la pila

elementopila Pop(pila *p)
{
    nodopila *temp;
    if(PilaVacia(p)==True)
    {
        printf("\nLa pila esta vacia no se puede adquirir nada \n");
    }
    else
    {
        temp=p->tope;
        free(p->tope);
        p->tope=temp->abajo;
        p->tamano--;

        return temp->objetopila;
    }
}
/*
 *agrega un elemento a la pila
 */
void Push(pila *p,elementopila nucleo)
{
    nodopila *temp;
    temp=CrearNodoPila();
    if(PilaVacia(p)==True)
    {
        //temp->abajo=p.base;//checar aqui porque no adquiero la direccion de memoria de la variable base ;
        //p->base=temp;
        temp->objetopila=nucleo;
        p->tope=temp;
        temp->abajo=p->apu_base;
        p->tamano++;
    }
    else
    {
        temp->objetopila=nucleo;
        temp->abajo=p->tope;
        p->tope=temp;
        p->tamano++;

    }
    //p->tamano++;
}
/*
 *verifiaca si la pila esta vacia regresando como valor verdadero cuando la pila esta vacia
 */
int PilaVacia(pila *p)
{
    if(p->tope==p->apu_base && p->tamano==0)//igual aqui checar la direccion de memoria
        return True;
    else
        return False;
}
/*
 * adquiere el tamaño de la pila o en numero de elementos que tiene la pila
 */
int Tam_pila(pila *p)
{
    return p->tamano;
}
/*
 *Crea un pila para su uso
 */
void CrearPila(pila *p)
{
    //p->base=NULL;
    //p->tope=NULL;
    p->tamano=0;
    p->apu_base=&p->base;//aqui requiero extraer la direccion de memoria
    p->tope=p->apu_base;
}
/*
 *genera nodos para ser insertados
 */
nodopila* CrearNodoPila(void)
{
    nodopila *temp;
    temp=malloc(sizeof(nodopila));
    temp->abajo=NULL;
    return temp;
}

