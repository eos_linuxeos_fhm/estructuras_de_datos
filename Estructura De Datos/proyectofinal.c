/*
 * File:   proyectofinal.c
 * Author: eos
 *
 * Created on 2 de julio de 2012, 09:47 AM
 */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "Elemento.h"
#include "Lista.h"
#include "arboles.h"
#include "Pila.h"
void imprilista(lista *l);
void coincidencias(lista *listatexto1,lista *listatexto2);//genera las coincidencias i las almacena en el nodo correspondiente
void imprilistacoincidencia(lista *l);
void ordenarlista(lista *l);//se ordena la lista por numero de coincidencias de menor a mayor
void logo(void);
nodoarbol* Arbolear(lista *l);
void tabla(nodoarbol *r);
/*
 *
 */
void tabla(nodoarbol *r)
{
    arbol arbol1;
    
    CrearArbol(&arbol1);    
    arbol1.raiz=r;
    
    
}
nodoarbol* Arbolear(lista *l)
{
    arbol arbol1, arbol2, arbol3;
    pila cuidareferencias;
    nodo *referencialista;
    elementoarbol temporalarbol1,temporalarbol2,temporalarbol3;
    elementopila contenedorarbol;
    int tam,indice;
    
    CrearPila(&cuidareferencias);
    CrearArbol(&arbol1);
    CrearArbol(&arbol2);
    CrearArbol(&arbol3);
    
    
    referencialista=Ultimo(l);
    tam=TamLista(l);
    
    for(indice=0;indice<tam;indice++)
    {
        if(Anterior(l,referencialista)->atras==&l->CERO)//para evitar que se siga el apuntador 
        {
            indice=tam;
        }
        temporalarbol3.sumado=referencialista->objeto.veces;
        temporalarbol3.rotulo=referencialista->objeto.caracter;            
        Construir(&arbol3,NULL,temporalarbol3,NULL);
            
        referencialista=Anterior(l,referencialista);
            
        temporalarbol2.sumado=referencialista->objeto.veces;
        temporalarbol2.rotulo=referencialista->objeto.caracter;            
        Construir(&arbol2,NULL,temporalarbol2,NULL);
            
            
        temporalarbol1.sumado=temporalarbol2.sumado+temporalarbol3.sumado;
        Construir(&arbol1,&arbol2,temporalarbol1,&arbol3);
            
        //contenedorarbol.root=arbol1.raiz;
        contenedorarbol.root=Raiz(&arbol1);
        Push(&cuidareferencias,contenedorarbol);
    }
    
    tam=Tam_pila(&cuidareferencias);
    for(indice=0;indice<tam;indice++)
    {
        if(Tam_pila(&cuidareferencias)==1)
        {
            indice=tam;
        }
        else
        {
            
            arbol3.raiz=(Pop(&cuidareferencias)).root;
            arbol2.raiz=(Pop(&cuidareferencias)).root;
            
            temporalarbol1.sumado=arbol3.raiz->objeto.sumado+arbol2.raiz->objeto.sumado;
            Construir(&arbol1,&arbol2,temporalarbol1,&arbol3);
            
            contenedorarbol.root=arbol1.raiz;
            Push(&cuidareferencias,contenedorarbol);
        }
    }
    
    arbol1.raiz=(Pop(&cuidareferencias)).root;
    
    
    return Raiz(&arbol1);
}

main()
{
    elementolista nucleo;
    lista listatexto,listarepe;//listas lista que se uso en el texto y lista con las repeticiones 
    nodoarbol *superaiz;
    char c;
    FILE *Escrito;
    int indice=0;
    system("clear");
    //printf("inicialisa listas\n");
    InicializarLista(&listatexto);
    InicializarLista(&listarepe);
    //printf("leer archivo\n");
    Escrito=fopen("texto.txt","r");
    //printf("inicia ciclo\n");
    while((c=getc(Escrito))!=EOF)
    {
        if((toascii(c))==10)//para ver saltos de carro
        {
            
            //printf("checando salto de carro %d **** %c",(toascii(c)),c);
            c='_';
        }
        nucleo.caracter=c;
        //printf("%c",nucleo.caracter);
        InsertarFinal(&listatexto,nucleo);
        indice++;
    }
    //printf("tamaño %d",TamLista(&listatexto));
    //imprilista(&listatexto);
    //puts("checando coincidecias");
    coincidencias(&listatexto,&listarepe);
    //puts("imprime lista de  coincidencias");
    imprilistacoincidencia(&listarepe);
    puts("entrando a la funcion de ordenando");
    ordenarlista(&listarepe);
    puts("imprime lista de coincidencias ordenada");
    imprilistacoincidencia(&listarepe);
    
    puts("arbolea");
    superaiz=Arbolear(&listarepe);
    puts("genera la tabla");
    tabla(superaiz);
    
    logo();
    puts("fin proyecto");





}
void ordenarlista(lista *l)//ordenamiento de coincidencias
{
    nodo *temporal,*contiguo,*temporal2;
    int tam,tam2,indice, indice2;
    temporal=Primero(l);
    temporal2=Ultimo(l);
    tam=TamLista(l);
    for(indice=0;indice<(tam);indice++)
    {
        tam2=TamLista(l);
        for(indice2=0;indice2<(tam2);indice2++)
        {
            
            
            //puts("-----ciclo_interno------");
            //puts("0.5");
            //contiguo=Siguiente(l,temporal);
            //puts("*1*");
            if((temporal->objeto.veces)==(Siguiente(l,temporal)->objeto.veces))
            {
                //puts("1.5");
                temporal=Siguiente(l,temporal);
                //puts("2");
                //indice2=tam2;
                //imprilistacoincidencia(l);
            }
            else if((temporal->objeto.veces)<(Siguiente(l,temporal)->objeto.veces))
            {
                temporal=Siguiente(l,temporal);
                ///puts("3");
                //indice2=tam2;
                //imprilistacoincidencia(l);
            }
            else if((temporal->objeto.veces)>(Siguiente(l,temporal)->objeto.veces)&&(temporal->adelante !=&l->CERO))
            {
                InsertarFrente(l,Siguiente(l,temporal),(temporal->objeto));
                temporal=Siguiente(l,temporal);
                
                Suprimir(l,Anterior(l,temporal));
                temporal=Siguiente(l,temporal);
                
                
               // puts("4");
                //contiguo=Siguiente(l,contiguo);
               // puts("5");
                //temporal=Anterior(l,contiguo);
                //puts("9");
               // indice2=tam2;
                //imprilistacoincidencia(l);
            }
            else
            {
                ;
            }
        }            
        //////////////////////////////////////////////////////////////////////////////

        for(indice2=0;indice2<(tam2);indice2++)
        {
            if((Anterior(l,temporal2)->objeto.veces)>(temporal2->objeto.veces)&& (temporal2->atras != &l->CERO))
            {
                //puts("1");
                InsertarAtras(l,Anterior(l,temporal2),temporal2->objeto);
                //puts("1.2");
                temporal2=Anterior(l,temporal2);
                //puts("1.3");
                Suprimir(l,Siguiente(l,temporal2));
                //puts("1.4");
                //temporal2=Anterior(l,temporal2);
            }
            else if((Anterior(l,temporal2)->objeto.veces)==(temporal2->objeto.veces)&& (temporal2->atras != &l->CERO))
            {
                //puts("2");
                temporal2=Anterior(l,temporal2);
            }
            else if((Anterior(l,temporal2)->objeto.veces)<(temporal2->objeto.veces)&& (temporal2->atras != &l->CERO))
            {
                //puts("3");
                temporal2=Anterior(l,temporal2);
            }
            else
            {
                ;
            }
            //puts("8.88");
        }
        //puts("\nchecando lista\n");
        temporal=Primero(l);
        temporal2=Ultimo(l);
        //imprilistacoincidencia(l);
        //puts("\nciclo_externo\n");        
    }    
/*
    
            puts("-----ciclo_interno------");
            puts("0.5");
            contiguo=Siguiente(l,temporal);
            puts("*1*");
            if((temporal->objeto.veces)==(contiguo->objeto.veces))
            {
                temporal=Siguiente(l,temporal);
                puts("2");
                indice2=tam2;
                //imprilistacoincidencia(l);
            }
            else if((temporal->objeto.veces)<(contiguo->objeto.veces))
            {
                temporal=Siguiente(l,temporal);
                puts("3");
                indice2=tam2;
                //imprilistacoincidencia(l);
            }
            else if((temporal->objeto.veces)>(contiguo->objeto.veces))
            {
                InsertarFrente(l,contiguo,(temporal->objeto));
                Suprimir(l,temporal);
                puts("4");
                contiguo=Siguiente(l,contiguo);
                puts("5");
                temporal=Anterior(l,contiguo);
                puts("9");
                indice2=tam2;
                //imprilistacoincidencia(l);
            }
            else
            {
                ;
            }
            
            puts("*1.0*");
            if(Anterior(l,temporal2)->objeto.caracter>temporal2->objeto.caracter)
            {
                puts("9.0");
                InsertarAtras(l,Anterior(l,temporal2),temporal2->objeto);
                puts("9.1");
                temporal2=Anterior(l,temporal2);
                puts("9.2");
                Suprimir(l,Siguiente(l,temporal2));
                puts("2.9");
                indice2=tam2;
            }
            else if(temporal2->objeto.caracter==Anterior(l,temporal2)->objeto.caracter)
            {
                puts("10");
                temporal2=Anterior(l,temporal2);
                puts("3.9");
                indice2=tam2;
            }
            else if(temporal2->objeto.caracter>Anterior(l,temporal2)->objeto.caracter)
            {
                puts("11");
                temporal2=Anterior(l,temporal2);
                puts("4.9");
                indice2=tam2;
            }
            else
            {
                ;
            }
            puts("8.88");
*/
/*
            else if((temporal2->objeto.caracter)==(Anterior(l,temporal2)->objeto.caracter))
            {
                //puts("10");
                temporal2=Anterior(l,temporal2);
                //puts("3.9");
                
            }
            else if((Anterior(l,temporal2)->objeto.caracter)<temporal2->objeto.caracter)
            {
                //puts("11");
                temporal2=Anterior(l,temporal2);
                //puts("4.9");
                
            }
            else
            {
                ;
            }
*/            
/*
            if(temporal2->atras==temporal)
            {
                puts("50.2");
                temporal2=Anterior(l,temporal);
            }
            else
            {
                ;
            }
            ///////////////////////////////////////////////////////////////////
            
           ///////////////////////////////////////////////////////////////////////////
            //puts("*1.0*");
*/
            
            
     
}
void imprilista(lista *l)
{
    nodo *referencia;
    int indice1,tam;
    referencia=Primero(l);
    tam=TamLista(l);
    puts(" ");
    for(indice1=0;indice1<tam;indice1++)
    {
        printf("%c",referencia->objeto.caracter);
        //puts("\n**elemento siguiente**");
        referencia=Siguiente(l,referencia);
    }
    puts(" ");


}
void imprilistacoincidencia(lista *l)
{
    nodo *referencia;
    int indice1,tam;
    referencia=Primero(l);
    tam=TamLista(l);
    puts(" ");
    for(indice1=0;indice1<tam;indice1++)
    {
        printf("%c,%d\n",referencia->objeto.caracter,referencia->objeto.veces);
        //puts("\n**elemento siguiente**");
        referencia=Siguiente(l,referencia);
    }
/*
    puts("************************************");
    referencia=Ultimo(l);
    for(indice1=0;indice1<tam;indice1++)
    {
        printf("%c,%d\n",referencia->objeto.caracter,referencia->objeto.veces);
        //puts("\n**elemento siguiente**");
        referencia=Anterior(l,referencia);
    }
    puts(" ");
*/


}
//lista 1 es la del texto completo lista 2 es la que genera las comparaciones
void coincidencias(lista *listatexto1,lista *listatexto2)
{
    //generacion de coincidencias
    int indice,indice2,contador=0,tam,tam2;
    char caracter1,caracter2;
    nodo *indicador,*indicador2;
    elementolista temporal,cara1,cara2,temp;
    tam=TamLista(listatexto1);

    indicador=Primero(listatexto1);

    temp=indicador->objeto;
    //printf("%c",temp.caracter);
    InsertarFinal(listatexto2,temp);
    indicador2=Primero(listatexto2);
    indicador2->objeto.veces=0;



    for(indice=0;indice<tam;indice++)
    {
        tam2=TamLista(listatexto2);
        //printf("\ntamaño lista2 vuelta valor indice:%d,,,tamaño%d\n",indice,tam2);
        for(indice2=0;indice2<=tam2;indice2++)
        {
            cara1=indicador->objeto;//el caracter del texto original
            caracter1=cara1.caracter;

            cara2=indicador2->objeto;//el de la lista que se esta generando
            caracter2=cara2.caracter;
            //indicador->objeto.caracter==indicador2->objeto.caracter;
            if(caracter1==caracter2)//si esta en conincidencia con el elemento
            {
                indicador2->objeto.veces++;
                indice2=(tam2+10);
                //indicador2=Siguiente(listatexto2,indicador2);
            }
            else if(contador==tam2)//si ya se termino la lista y no aparece para agregarlo
            {
                temporal.caracter=indicador->objeto.caracter;
                temporal.veces=1;
                InsertarFinal(listatexto2,temporal);
                indice2=(tam2+10);
            }
            else
            {
                indicador2=Siguiente(listatexto2,indicador2);
                contador++;
            }
        }
        indicador=Siguiente(listatexto1,indicador);
        contador=1;
        indicador2=Primero(listatexto2);
    }
}
void logo(void)
{
    printf("\n\nFERNANDO HERNANDEZ MARTINEZ\n\n");
    printf("      .--.         \n");
    printf("     |o_o |        \n");
    printf("     |:_/ |        \n");
    printf("    //    \\\\     \n");
    printf("   (|      |)      \n");
    printf("  /'\\ _   _/`\\   \n");
    printf("  \\ ___)=(___/    \n");
    printf("******************\n");
}
/*
    
    printf("\n\ntamaño de la lista %d\n\n",tam);
    printf("temporal2 apunta a : %c,%d\n",temporal2->objeto.caracter,temporal2->objeto.veces);
    while(temporal2->adelante!=&l->CERO)
    {
        temporal=Primero(l);
        for(indice=0;indice<tam;indice++)
        {
            if((temporal->objeto.veces)>(Siguiente(l,temporal)->objeto.veces))//ver si el temporal es mayor que el nodo siguente ;
            {
                puts("generando intercambio el elemento es mayor");
                printf("antes de intercambio temporal apunta a : %c,%d\n",temporal->objeto.caracter,temporal->objeto.veces);
                printf("*********ordenando %c,%d\n",temporal->objeto.caracter,temporal->objeto.veces);
                InsertarFrente(l,Siguiente(l,temporal),temporal->objeto);
                temporal=Siguiente(l,temporal);
                imprilistacoincidencia(l);
                printf("antes del borrado temporal apunta a: %c,%d\n",temporal->objeto.caracter,temporal->objeto.veces);
                Suprimir(l,Anterior(l,temporal));
                //temporal=Siguiente(l,temporal);
                printf("despues del borrado temporal apunta a : %c,%d\n",temporal->objeto.caracter,temporal->objeto.veces);
                printf("anterior del temporal apunta a : %c,%d\n",Anterior(l,temporal)->objeto.caracter,Anterior(l,temporal)->objeto.veces);
                printf("siguente del temporal apunta a : %c,%d\n",Siguiente(l,temporal)->objeto.caracter,Siguiente(l,temporal)->objeto.veces);
                imprilistacoincidencia(l);
                indice=tam;
            }
            else
            {
                puts("saltando elemento no es mayor");
                printf("antes de cambio temporal apunta a : %c,%d\n",temporal->objeto.caracter,temporal->objeto.veces);
                temporal=Siguiente(l,temporal);
                printf("despues del cambio temporal apunta a : %c,%d\n",temporal->objeto.caracter,temporal->objeto.veces);
                imprilistacoincidencia(l);
                
            }
        }
        
        printf("paso por un ciclo\n\n");
        temporal2=Siguiente(l,temporal2);
        printf("temporal2 apunta a : %c,%d\n",temporal2->objeto.caracter,temporal2->objeto.veces);
    }
*/
/*
            else if(temporal->adelante==Ultimo(l))
            {
                temporal->adelante=&l->CERO;
            }
*/
