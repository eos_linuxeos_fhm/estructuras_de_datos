/* 
 * File:   DoblePila.c
 * Author: eos
 *hacer un programa para implantar dor pilas usando solo un arreglo con sus
 *respectivas manipulaciones no debe declararse desbordamiento a menos que se 
 *hayan usado todas las posiciones del arreglo
 * Created on 2 de septiembre de 2012, 12:03 PM
 */

#include <stdio.h>
#include <stdlib.h>
#define MAX 10
#define TRUE 1
#define FALSE 0
#define derecha 1
#define izquierda 2

typedef struct Elemento
{
    int dato;
}elemento;
typedef struct Nodo
{
    elemento nucleo;
}nodo;
typedef struct Pilas
{
    int tam_p1;
    int tam_p2;
    int p1;//derecha indica que posicion lleva
    int p2;//izquierda indica que posicion lleva
    nodo SuperPila[MAX];
}pilas;
void Push(pilas *p,elemento info,int donde);
elemento Pop(pilas *p,int donde);
int Tamanos(pilas *p,int donde);
void CreaPilas(pilas *p);
int PilasLlena(pilas *p);
int PilasVacias(pilas *p,int donde);//se le manda la informacion de que pila sera checada
void MostrarPilas(pilas *p);
/******************************************************************************/
/*
 * uso de la pila doble 
 */
main()
{
    int opt=0,opt2=0;
    elemento info,datito;
    pilas acumulador;
    CreaPilas(&acumulador);
    while(opt!=9)
    {
        puts("seleccione la opcion correcta");
        puts("1.- Guardar en la derecha");
        puts("2.- Guardar en la izquierda");
        puts("3.- Mostrar Pilas");
        puts("4.- Tamaños de las pilas");
        puts("5.- Extraer Dato");
        puts("9.- Salir");
        scanf("%d",&opt);
        //9scanf("%d",&opt);
        if(opt==1)
        {
            puts("ingrese su valor para ser insertado en la derecha");
            scanf("%d",&info.dato);                    
            Push(&acumulador,info,derecha);
        }
        else if(opt==2)
        {
            puts("ingrese su valor para ser insertado en la izquierda");
            scanf("%d",&info.dato);
            Push(&acumulador,info,izquierda);
        }
        else if(opt==3)
        {
            MostrarPilas(&acumulador);
            printf("<-Tope;\n\n");
            printf("checando valor del error %d\n\n",acumulador.SuperPila[MAX].nucleo.dato);
        }
        else if(opt==4)
        {
            printf("tamaño de la pila derecha %d\n",Tamanos(&acumulador,derecha));
            printf("tamaño de la pila izquierda %d\n",Tamanos(&acumulador,izquierda));
            printf("Tamaño restante para las pilas %d \n\n",(MAX-(Tamanos(&acumulador,derecha)+Tamanos(&acumulador,izquierda))));
        }
        else if(opt==5)
        {
            printf("        SubMenu\n");
            printf("        Seleccione la opcion correcta\n");
            printf("        1.- Extraer de la derecha\n");
            printf("        2.- Extraes de la izquierda\n        ");
            scanf("%d",&opt2);
            if(opt2==1 || opt2==2)
            {
                datito=Pop(&acumulador,opt2);
                printf("             dato extraido %d \n",datito.dato);
            }
            else
            {
                printf("             seleccion erronea\n\n");
            }
        }
        else if(opt==9)
        {
            puts("adios");
        }
        else
        {
            puts("error de seleecion intentelo otra vez");
        }
    }
    
    
}
/******************************************************************************/
void MostrarPilas(pilas *p)
{
    int indice,datote=0,indice2=MAX;
    
    printf("\nelementos pila derecha      ->");    
    for(indice=0;indice<p->tam_p1;indice++)
    {
        datote=p->SuperPila[indice].nucleo.dato;
        printf("%d ",datote);
        
    }
    
    
    printf("<-Tope;\nelementos pila izquierda    ->");    
    for(indice=0;indice<p->tam_p2;indice++)
    {
        datote=p->SuperPila[indice2].nucleo.dato;
        printf("%d ",datote);
        indice2--;
    }
}
/******************************************************************************/
/******************************************************************************/
void Push(pilas *p,elemento info,int donde)
{
    if(PilasLlena(p)==TRUE)
    {
        puts("error pilas llenas\n\n");
    }
    else
    {
        if(donde == derecha)
        {
            //p->p1=p->p1++;
            p->SuperPila[p->p1].nucleo=info;
            p->p1=p->p1++;
            p->tam_p1=p->tam_p1+1;
        }
        else if(donde ==izquierda)
        {
            //p->p2=p->p2--;
            p->SuperPila[p->p2].nucleo=info;
            p->p2=p->p2--;
            p->tam_p2=p->tam_p2+1;            
        }
        else
        {
            puts("error no definiste en que pila sera guardado el elemento");
        }
    }
    
}
elemento Pop(pilas *p,int donde)
{
    elemento tem;
    if(PilasVacias(p,donde)==TRUE)
    {
        puts("error pila vacia");
    }
    else
    {
        if(donde == derecha)
        {
            tem= p->SuperPila[p->p1].nucleo;
            p->tam_p1=p->tam_p1--;
            p->p1=p->p1-1;
            return tem;
        }
        else if(donde == izquierda)
        {
            tem= p->SuperPila[p->p2].nucleo;
            p->tam_p2=p->tam_p2--;
            p->p2=p->p2-2;
            return tem;
        }
        else
        {
            puts("error no definiste la pila bien");
        }            
    }
    
}
int Tamanos(pilas *p,int donde)
{
    if(donde ==derecha)
    {
        return p->tam_p1;
    }
    else if(donde==izquierda)
    {
        return p->tam_p2;
    }
    else
    {
        puts("Error no es correcta la pila");
    }
        
}
void CreaPilas(pilas *p)
{
    p->tam_p1=0;
    p->tam_p2=0;
    p->p1=0;
    p->p2=MAX;    
}
int PilasLlena(pilas *p)
{
    if(Tamanos(p,derecha)+Tamanos(p,izquierda)==MAX)
    {
        return TRUE;
    }
    else 
    {
        return FALSE;
    }
}
int PilasVacias(pilas *p,int donde)
{
    if(donde==derecha)
    {
        if(p->p1==0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }        
    }
    else if(donde ==izquierda)
    {
        if(p->p2==MAX)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    else
    {
        puts("error no definiste cual pila");
    }
        
}
/******************************************************************************/